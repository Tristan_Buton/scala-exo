class Book(libelle: String, price: Int) {

  override def toString: String = {
    return this.libelle + ", " + this.price + "€"

  }

  def getPrice: Int = {
    return price
  }
}
