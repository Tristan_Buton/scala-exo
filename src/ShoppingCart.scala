
class ShoppingCart {

  var map:Map[Book, Int] = Map.empty[Book, Int]

  def adjust[A, B](m: Map[A, B], k: A)(f: B => B) = m.updated(k, f(m(k)))

  def add(book: Book): Map[Book, Int] = {
    if(this.map.contains(book)){
      this.map = adjust(this.map, book)(_ + 1)
      return this.map
    }
    else
      this.map += (book -> 1)
      return this.map
  }

  override def toString: String = {
    this.map.toString()
  }

  def getPriceWithoutDiscount: Int = {
    var priceWoD: Int = 0
    for ((k,v) <- this.map)
      priceWoD += k.getPrice * v
    return priceWoD
  }

  // 2 diff -> 5%
  // 3 diff -> 10%
  // 4 diff -> 20%
  // 5 diff -> 25%
  def getPriceWithDiscount: Double = {

    var nbBook = 0
    for ((k,v) <- this.map)
      nbBook += v
    println(nbBook)
    var price:Double = 0.0
    while (!this.map.isEmpty) {
      println(this.map)
      val (discount, tmpPrice) = calculDiscount
      price += tmpPrice - (discount * tmpPrice / 100)
      println(price)
    }

    return price
  }


  def calculDiscount: (Double,Double) =  {
  var discount = new Discount(this.map.size).apply
  var tmpPrice = 0.0
  for ((k,v) <- this.map){
    tmpPrice += k.getPrice
    this.map = adjust(this.map, k)(_ - 1)
      if (this.map(k) == 0){
        this.map = this.map - k
      }
    }
  val tuppleRet = (discount, tmpPrice)
  return tuppleRet
  }
}

