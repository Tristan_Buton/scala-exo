object Main extends App {
  println("hello word !")
  val b1 = new Book("GOT tome 1", 8)
  val b2 = new Book("GOT tome 2", 8)
  val b3 = new Book("GOT tome 3", 8)
  val b4 = new Book("GOT tome 4", 8)
  val b5 = new Book("GOT tome 5", 8)
  println(b1.toString)

  val sc = new ShoppingCart()
  sc.add(b1)
  sc.add(b1)
  sc.add(b2)
  sc.add(b2)
  sc.add(b3)
  sc.add(b3)
  sc.add(b4)
  sc.add(b5)

  println(sc)
  //println(sc.getPriceWithoutDiscount)
  println(sc.getPriceWithDiscount)
}
