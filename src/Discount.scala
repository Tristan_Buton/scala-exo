class Discount(nb: Double) {
  def apply: Double = {
    nb match {
      case 2 => 5.0
      case 3=> 10.0
      case 4 => 20.0
      case 5 => 25.0
      case _ => 0.0
    }
  }
}
